export default {
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.json',
        },
    },
    moduleFileExtensions: [
        'ts',
        'js',
    ],
    globalTeardown: './test/jest.teardown.ts',
    testMatch: [
        '**/test/**/*.test.(ts|js)',
    ],
    transform: {
        '^.+\\.(ts|js|tsx)$': 'ts-jest',
    },
    transformIgnorePatterns: [
        '<rootDir>/node_modules/(?!@foo)',
    ],
    testEnvironment: 'node',
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
    },
    testTimeout: 30000,
};
