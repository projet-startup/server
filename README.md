> Février 2021

# BackEnd du projet StartUp POC (FISE3)
Projet du group A1 : **_Corentin Ferlay, Julien Giovianzzo, Malo Saunier, et Kenza Yahiaoui_**

## Technologies Back
Les technologies utilisées sont :
    - un server node JS avec les frameworks TypeORM et Express js
    - un base de donnée MySQL
Pour lancer le serveur en local : `npm run watch`.
Pour lancer le serveur de test en local : `npm run test`.

## Projet Front
Vous trouverez le repository de notre frontend ici: https://gitlab.com/projet-startup/webapp 

