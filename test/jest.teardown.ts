import { close } from '../src/core/db-connections';

export default async function closeCOnnection() {
    close().catch((err) => console.error(err));
}
