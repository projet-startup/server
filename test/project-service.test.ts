import { Project } from '../src/models/Project';
import projectService from '../src/services/project-service';
import { v4 as uuid } from 'uuid';
import { ConflictError, NotFoundError } from '../src/core/error/http-error';

describe('ProjectService', () => {
    const id = uuid();
    const project = new Project(id, `Test${id}`, 'test', true);

    test('Project is created and found', async () => {
        project.id = await projectService.create(project);
        const proj = await projectService.findById(project.id);
        expect(proj).toStrictEqual(project);
    });

    /* test('Project is duplicate', async () => {
        await expect(projectService.create(project)).rejects.toThrowError(ConflictError);
    });  */

    test('Project is updated', async () => {
        project.description = 'Updated';
        project.name = 'Updated';
        await projectService.update(project);
        const proj = await projectService.findById(project.id);
        expect(proj).toMatchObject(project);
    });

    test('Project is deleted then not found', async () => {
        await projectService.delete(project.id);
        await expect(projectService.findById(project.id)).rejects.toThrowError(NotFoundError);
    });
});
