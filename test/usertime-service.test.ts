import { User } from '../src/models/User';
import { v4 as uuid } from 'uuid';
import { Project } from '../src/models/Project';
import { UserTime } from '../src/models/UserTime';
import userService from '../src/services/user-service';
import projectService from '../src/services/project-service';
import usertimeService from '../src/services/usertime-service';
import { NotFoundError } from '../src/core/error/http-error';

describe('UserTimeController', () => {
    const dev = new User(uuid(), 'Test', 'Test', `test${uuid().substr(0, 10)}@test.com`, 'test', 'DEV');
    const project = new Project(uuid(), `Test${uuid().substr(0, 10)}`, 'test', true);
    let usertime: UserTime;
    afterAll(async () => {
        await userService.delete(dev.id);
        await projectService.delete(project.id);
        await usertimeService.delete(usertime.id);
    });
    test('Usertime is created', async () => {
        dev.id = await userService.create(dev);
        project.id = await projectService.create(project);
        await userService.addToProject(dev.id, project.id);
        const usertime2 = new UserTime(uuid(), 4, new Date(), false, dev.id, project.id);
        usertime2.id = await usertimeService.create(usertime2);
        usertime = usertime2;
        const usertimeFound = await usertimeService.findById(usertime2.id);
        expect(usertimeFound.userId).toEqual(usertime.userId);
        expect(usertimeFound.time).toEqual(4);
    });

    test('Usertime is updated', async () => {
        usertime.date = new Date('21/02/2021');
        usertime.time = 2;
        await usertimeService.update(usertime);
        const usertimeFound = await usertimeService.findById(usertime.id);
        expect(usertimeFound.time).toEqual(2);
    });

    test('Usertime is deleted and then not found', async () => {
        await usertimeService.delete(usertime.id);
        await expect(usertimeService.findById(usertime.id)).rejects.toThrowError(NotFoundError);
    });
});
