import { User } from '../src/models/User';
import { v4 as uuid } from 'uuid';
import userService from '../src/services/user-service';
import { ConflictError, NotFoundError } from '../src/core/error/http-error';
import { Project } from '../src/models/Project';
import projectService from '../src/services/project-service';

describe('UserService', () => {
    const admin = new User(uuid(), 'Test', 'Test', `test${uuid().substr(0, 10)}@test.com`, 'test', 'ADMIN');
    const dev = new User(uuid(), 'Test', 'Test', `test${uuid().substr(0, 10)}@test.com`, 'test', 'DEV');
    const manager = new User(uuid(), 'Test', 'Test', `test${uuid().substr(0, 10)}@test.com`, 'test', 'MANAGER');
    const project = new Project(uuid(), 'projet_test', 'test description', true);

    afterAll(async () => {
        await userService.delete(admin.id);
        await userService.delete(dev.id);
        await userService.delete(manager.id);
        await projectService.delete(project.id);
    });

    test('User is created and find', async () => {
        admin.id = await userService.create(admin);
        dev.id = await userService.create(dev);
        manager.id = await userService.create(manager);
        const userReturnedAdmin = await userService.findById(admin.id);
        const userReturnedDev = await userService.findById(dev.id);
        const userReturnedManager = await userService.findById(manager.id);
        const matchReturnedAdmin = {
            email: admin.email, firstname: admin.firstname, lastname: admin.lastname, projects: [], managerId: null,
        };
        const matchReturnedDev = {
            email: dev.email, firstname: dev.firstname, lastname: dev.lastname, projects: [], managerId: null,
        };
        const matchReturnedManager = {
            email: manager.email, firstname: manager.firstname, lastname: manager.lastname, projects: [], managerId: null,
        };
        expect(userReturnedAdmin).toMatchObject(matchReturnedAdmin);
        expect(userReturnedDev).toMatchObject(matchReturnedDev);
        expect(userReturnedManager).toMatchObject(matchReturnedManager);
    });

    test('User is duplicate', async () => {
        await expect(userService.create(admin)).rejects.toThrowError(ConflictError);
    });

    test('User is updated', async () => {
        const matchModified = {
            email: 'test@modified.com', firstname: 'modified', lastname: 'modified',
        };
        admin.email = matchModified.email;
        admin.firstname = matchModified.firstname;
        admin.lastname = matchModified.lastname;
        await userService.update(admin);
        const modifiedUser = await userService.findById(admin.id);
        expect(modifiedUser).toMatchObject(matchModified);
    });

    test('User is deleted then not find', async () => {
        await userService.delete(admin.id);
        await expect(userService.findById(admin.id)).rejects.toThrowError(NotFoundError);
    });

    test('User is added a manager', async () => {
        await userService.setManager(dev.id, manager.id);
        const returnedDev = await userService.findById(dev.id);
        expect(returnedDev.managerId).toEqual(manager.id);
    });

    test('User is added a project', async () => {
        project.id = await projectService.create(project);
        await userService.addToProject(dev.id, project.id);
        const returnedDev = await projectService.findDevelopers(project.id);
        expect(returnedDev[0].id).toEqual(dev.id);
    });

    test('User is removed a project', async () => {
        await userService.removeFromProject(dev.id, project.id);
        const returnedDev = await projectService.findDevelopers(project.id);
        expect(returnedDev.length).toEqual(0);
    });

    test('Role setted to a no existing user ', async () => {
        await expect(userService.setRole(uuid(), 'ADMIN')).rejects.toThrowError(NotFoundError);
    });

    test('Role setted to a User', async () => {
        await userService.setRole(dev.id, 'ADMIN');
        const returnedUser = await userService.findById(dev.id);
        expect(returnedUser.role).toEqual('ADMIN');
    });

    test('User is logged after password change', async () => {
        let logresult = await userService.login(dev.email, dev.password);
        expect(logresult.token).toBeTruthy();
        const newPass = 'test2';
        await userService.setPassword(dev.id, newPass);
        dev.password = newPass;
        logresult = await userService.login(dev.email, dev.password);
        expect(logresult.token).toBeTruthy();
    });
});
