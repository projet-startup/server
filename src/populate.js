import { User } from './models/User';
import userService from './services/user-service';
import projectService from './services/project-service';
import { v4 as uuid } from 'uuid';
import { Project } from './models/Project';

async function init() {
    const admin = new User(uuid(), 'Admin', 'Test', 'test@admin.com', 'test', 'ADMIN');
    const manager = new User(uuid(), 'Manager', 'Test', 'test@manager.com', 'test', 'MANAGER');
    const dev = new User(uuid(), 'Dev', 'Test', 'test@dev.com', 'test', 'DEV');
    admin.id = await userService.create(admin);
    manager.id = await userService.create(manager);
    dev.id = await userService.create(dev);
    await userService.setManager(dev.id, manager.id);
    const project = new Project(uuid(), 'Projet initial', 'Projet par default', true);
    project.id = await projectService.create(project);
    await userService.addToProject(dev.id, project.id);
}

init().catch((err) => console.error(err));
