import {
    Router, Request, Response, NextFunction,
} from 'express';
import { Controller } from '../core/controller';
import { isAuthorized } from '../middleware/auth-middleware';
import { UserTime } from '../models/UserTime';
import usertimeService, { UserTimeService } from '../services/usertime-service';
import {
    isDateCorrect,
    isQueryCorrect, isUserTimeCorrect, isUUID,
} from '../validators';
import * as cfg from '../config';
import { NotFoundError } from '../core/error/http-error';
import fs from 'fs-extra';

export class UserController implements Controller {
    constructor(private userTimeService: UserTimeService) {}

    public initRoutes(router: Router) {
        router.get('/usertimes/user/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findByUser(req, res, next).catch(next));
        router.get('/usertimes/manager/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findByManager(req, res, next).catch(next));
        router.get('/usertimes/project/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findByProject(req, res, next).catch(next));
        router.get('/usertimes/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findById(req, res, next).catch(next));
        router.post('/usertimes',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.DEV]).catch(next),
            (req, res, next) => this.create(req, res, next).catch(next));
        router.post('/usertimes/validate/:uid',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.DEV]).catch(next),
            (req, res, next) => this.validate(req, res, next).catch(next));
        router.patch('/usertimes/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.update(req, res, next).catch(next));
        router.delete('/usertimes/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.delete(req, res, next).catch(next));
        router.get('/usertimes/:uid/PDF/:date',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.generatePDF(req, res, next).catch(next));
    }

    public async findAll(req: Request, res: Response, next: NextFunction) {
        const userTimes: UserTime[] = await this.userTimeService.findAll();
        res.send({ userTimes });
    }

    public async findById(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        const userTime = await this.userTimeService.findById(id);
        res.send({ ...userTime });
    }

    public async findByUser(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.id);
        const options = isQueryCorrect(req.query);
        if (options.active) {
            const projects = await this.userTimeService.findActiveByUser(uid, options);
            res.send({ projects });
        } else if (options.chrono) {
            const usertimes = await this.userTimeService.findByUserChrono(uid, options);
            res.send({ usertimes });
        } else {
            const projects = await this.userTimeService.findByUser(uid, options);
            res.send({ projects });
        }
    }

    public async findByManager(req: Request, res: Response, next: NextFunction) {
        const options = isQueryCorrect(req.query);
        const uid = isUUID(req.params.id);
        const users = await this.userTimeService.findByManager(uid, options);
        res.send({ users });
    }

    public async findByProject(req: Request, res: Response, next: NextFunction) {
        const options = isQueryCorrect(req.query);
        const uid = isUUID(req.params.id);
        const userTimes = await this.userTimeService.findByProject(uid, options);
        res.send({ ...userTimes });
    }

    public async create(req: Request, res: Response, next: NextFunction) {
        const userTime = isUserTimeCorrect(req.body);
        userTime.userId = req.user!.id;
        const id = await this.userTimeService.create(userTime);
        res.status(201).send({ id });
    }

    public async update(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        const usertime = isUserTimeCorrect(req.body);
        usertime.id = id;
        await this.userTimeService.update(usertime);
        res.send({ id });
    }

    public async delete(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        await this.userTimeService.delete(id);
        res.send({ id });
    }

    public async validate(req: Request, res: Response, next: NextFunction) {
        const { uid } = req.params;
        const affectedTime = await this.userTimeService.validate(uid, isDateCorrect(req.body.date));
        res.send({ affectedTime });
    }

    public async generatePDF(req: Request, res: Response, next: NextFunction) {
        const { uid } = req.params;
        const { date } = req.params;
        const file = await this.userTimeService.generatePDF(uid, isDateCorrect(date));
        if (!file) {
            throw new NotFoundError('No file found with the provided ID');
        }
        res.download(file, async (err) => {
            if (err) {
                console.error(err);
            }
            await fs.remove(file);
        });
    }
}

export default new UserController(usertimeService);
