import {
    Router, Request, Response, NextFunction,
} from 'express';
import { Controller } from '../core/controller';
import { isAuthorized } from '../middleware/auth-middleware';
import userService, { UserService } from '../services/user-service';
import { isLoginCorrect, isUserCorrect } from '../validators';
import * as cfg from '../config';

export class AuthController implements Controller {
    constructor(private userService: UserService) {}

    initRoutes(router: Router) {
        router.post('/login', (req, res, next) => this.login(req, res, next).catch(next));
        router.post('/register', (req, res, next) => isAuthorized(req, res, next, [cfg.roles.ADMIN]),
            (req, res, next) => this.login(req, res, next).catch(next));
    }

    async login(req: Request, res: Response, next: NextFunction) {
        const login = isLoginCorrect(req.body);
        const result = await userService.login(login.email, login.password);
        res.send(result);
    }

    async register(req: Request, res: Response, next: NextFunction) {
        const user = isUserCorrect(req.body);
        const token = await this.userService.create(user);
        res.send({ token, type: 'Bearer' });
    }
}

export default new AuthController(userService);
