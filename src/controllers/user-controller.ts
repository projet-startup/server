import {
    Router, Request, Response, NextFunction,
} from 'express';
import { Controller } from '../core/controller';
import { ForbiddenError, NotFoundError } from '../core/error/http-error';
import { isAuthorized } from '../middleware/auth-middleware';
import { User } from '../models/User';
import userService, { UserService } from '../services/user-service';
import {
    isNewPasswordCorrect, isRoleCorrect, isUserCorrect, isUserUpdateCorrect, isUUID,
} from '../validators';
import * as cfg from '../config';

export class UserController implements Controller {
    constructor(private userService: UserService) {}

    initRoutes(router: Router) {
        router.get('/users',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findAll(req, res, next).catch(next));
        router.get('/users/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findById(req, res, next).catch(next));
        router.post('/users',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.create(req, res, next).catch(next));
        router.delete('/users/:id/project/:projectId',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.MANAGER, cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.removeFromProject(req, res, next).catch(next));
        router.patch('/users/:id/:info',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.MANAGER, cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.update(req, res, next).catch(next));
        router.patch('/users/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.basicUpdate(req, res, next).catch(next));
        router.delete('/users/:id',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.delete(req, res, next).catch(next));
    }

    async findAll(req: Request, res: Response, next: NextFunction) {
        const users: User[] = await this.userService.findAll();
        res.send({ users });
    }

    async findById(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        const user = await this.userService.findById(id);
        res.send({ ...user });
    }

    async create(req: Request, res: Response, next: NextFunction) {
        const user = isUserCorrect(req.body);
        const id = await this.userService.create(user);
        res.status(201).send({ id });
    }

    async update(req: Request, res: Response, next: NextFunction) {
        const { id, info } = req.params;
        isUUID(id);
        if (info === 'manager') {
            let managerId = null;
            if (req.body.managerId !== '') {
                managerId = isUUID(req.body.managerId);
            }
            await this.userService.setManager(id, managerId);
        } else if (info === 'password') {
            await this.userService.setPassword(id, isNewPasswordCorrect(req.body));
        } else if (info === 'role') {
            await this.userService.setRole(id, isRoleCorrect(req.body.role));
        } else if (info === 'project') {
            const projectId = isUUID(req.body.projectId);
            await this.userService.addToProject(id, projectId);
        } else {
            throw new NotFoundError();
        }

        res.send({ id });
    }

    async basicUpdate(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        const user = isUserUpdateCorrect(req.body);
        user.id = id;
        await this.userService.update(user);
        res.send({ id });
    }

    async removeFromProject(req: Request, res: Response, next: NextFunction) {
        const { id, projectId } = req.params;
        isUUID(id);
        isUUID(projectId);
        await userService.removeFromProject(id, projectId);
        res.send({});
    }

    async delete(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        if (id === req.user?.id) {
            throw new ForbiddenError('Impossible de supprimer votre propre compte');
        }
        await this.userService.delete(id);
        res.send({ id });
    }
}

export default new UserController(userService);
