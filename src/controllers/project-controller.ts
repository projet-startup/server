import {
    Router, Request, Response, NextFunction,
} from 'express';
import { Controller } from '../core/controller';
import { isAuthorized } from '../middleware/auth-middleware';
import { Project } from '../models/Project';
import projectService, { ProjectService } from '../services/project-service';
import { isProjectCorrect, isUUID } from '../validators';
import * as cfg from '../config';

export class ProjectController implements Controller {
    constructor(private projectService: ProjectService) {}

    initRoutes(router: Router) {
        router.get('/projects',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findAll(req, res, next).catch(next));
        router.get('/projects/:id',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findById(req, res, next).catch(next));
        router.get('/projects/:id/developers',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findDevelopers(req, res, next).catch(next));
        router.post('/projects',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.MANAGER, cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.create(req, res, next).catch(next));
        router.patch('/projects/:id',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.MANAGER, cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.update(req, res, next).catch(next));
        router.delete('/projects/:id',
            (req, res, next) => isAuthorized(req, res, next, [cfg.roles.MANAGER, cfg.roles.ADMIN]).catch(next),
            (req, res, next) => this.delete(req, res, next).catch(next));
    }

    async findAll(req: Request, res: Response, next: NextFunction) {
        const projects: Project[] = await this.projectService.findAll();
        res.send({ projects });
    }

    async findById(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        const project = await this.projectService.findById(id);
        res.send({ ...project });
    }

    async create(req: Request, res: Response, next: NextFunction) {
        const project = isProjectCorrect(req.body);
        const id = await this.projectService.create(project);
        res.status(201).send({ id });
    }

    async update(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        const project = isProjectCorrect(req.body);
        project.id = id;
        await this.projectService.update(project);
        await this.projectService.changeOpen(project.id, project.isOpen);
        res.send({ id });
    }

    async delete(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        await this.projectService.delete(id);
        res.send({ id });
    }

    async findDevelopers(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        isUUID(id);
        const devs = await this.projectService.findDevelopers(id);
        res.send({ devs });
    }
}

export default new ProjectController(projectService);
