import Joi from 'joi';
import { NotFoundError, UnprocessableEntityError } from './core/error/http-error';
import { Project } from './models/Project';
import { User } from './models/User';
import { UserTime } from './models/UserTime';
import * as cfg from './config';

export function isLoginCorrect(body: any): {email: string, password: string} {
    const object = Joi.object({
        email: Joi.string().email().required().messages({
            'string.email': 'L\'email est incorrect',
        }),
        password: Joi.string().required(),
    }).messages({ 'any.required': 'Un champ obligatoire est manquant' }).options({ abortEarly: false });
    const result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isUserCorrect(body: any): User {
    const object = Joi.object<User>({
        email: Joi.string().email().required().messages({
            'string.email': 'L\'email est incorrect',
        }),
        firstname: Joi.string().min(2).max(20).required()
            .messages({
                'string.max': 'Le prénom est trop long, max: 20',
                'string.min': 'Le prénom  est trop court, min: 2',
            }),
        lastname: Joi.string().min(2).max(40).required()
            .messages({
                'string.max': 'Le nom de famille est trop long, max: 40',
                'string.min': 'Le nom de famille est trop court, min: 2',
            }),
        password: Joi.string().min(3).max(40).required()
            .messages({
                'string.max': 'Le mot de passe est trop long, max: 40',
                'string.min': 'Le mot de passe est trop court, min: 3',
            }),
        managerId: Joi.string().uuid().messages({
            'string.guid': 'L\'ID manager fournit n\'est pas correct',
        }),
        role: Joi.string().valid(cfg.roles.DEV, cfg.roles.ADMIN, cfg.roles.MANAGER).required()
            .messages({
                'any.valid': `Le rôle est incorrect, ce doit être un parmis ${cfg.roles.DEV}, ${cfg.roles.MANAGER} ou ${cfg.roles.ADMIN}`,
            }),
    }).messages({ 'any.required': 'Un champ obligatoire est manquant' }).options({ abortEarly: false });
    const result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isUserUpdateCorrect(body: any): User {
    const object = Joi.object<User>({
        email: Joi.string().email().required().messages({
            'string.email': 'L\'email est incorrect',
        }),
        firstname: Joi.string().min(2).max(20).required()
            .messages({
                'string.max': 'Le prénom est trop long, max: 20',
                'string.min': 'Le prénom  est trop court, min: 2',
            }),
        lastname: Joi.string().min(2).max(40).required()
            .messages({
                'string.max': 'Le nom de famille est trop long, max: 40',
                'string.min': 'Le nom de famille est trop court, min: 2',
            }),
    }).messages({ 'any.required': 'Un champ obligatoire est manquant' }).options({ abortEarly: false });
    const result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isProjectCorrect(body: any): Project {
    const object = Joi.object<Project>({
        name: Joi.string().min(3).max(20).required()
            .messages({
                'string.max': 'Le nom est trop long, max: 20',
                'string.min': 'Le nom est trop court, min: 3',
            }),
        description: Joi.string().max(250).required().messages({ 'string.max': 'La description est trop longue, max: 250' }),
        isOpen: Joi.bool().truthy(1).default(() => true).messages({
            'bool.base': '"isOpen" doit être un booléen',
        }),
    }).messages({ 'string.empty': 'Les champs ne doivent pas être vide' }).options({ abortEarly: false });
    const result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isUserTimeCorrect(body: any): UserTime {
    const object = Joi.object<UserTime>({
        date: Joi.date().required().messages({
            'date.base': 'La date fournit n\'est pas correct',
        }),
        time: Joi.number().integer().positive().required()
            .messages({
                'number.base': 'Le temps fournit n\'est pas correct',
                'number.integer': 'Le temps fournit n\'est pas un entier',
                'number.positive': 'Le temps fournit doit être positif',
            }),
        projectId: Joi.string().uuid().required(),
    }).messages({ 'any.required': 'Un champ obligatoire est manquant' }).options({ abortEarly: false });
    const result = object.validate(body, { convert: true });
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isQueryCorrect(query: any): {page: number, chrono: boolean, active: boolean, curMonth: boolean} {
    const object = Joi.object({
        page: Joi.number().integer().positive().default(() => 1)
            .messages({
                'number.base': '"page" doit être un entier positif',
                'number.integer': '"page" doit être un entier positif',
                'number.positive': '"page" doit être un entier positif',
            }),
        chrono: Joi.bool().truthy(1).default(() => false).messages({
            'bool.base': '"chrono" doit être un booléen',
        }),
        curMonth: Joi.bool().truthy(1).default(() => false).messages({
            'bool.base': '"curMonth" doit être un booléen',
        }),
        active: Joi.bool().truthy(1).default(() => false).messages({
            'bool.base': '"active" doit être un booléen',
        }),
    }).messages({ 'any.required': 'Un champ obligatoire est manquant' }).options({ abortEarly: false });
    const result = object.validate({
        chrono: query.chrono, page: query.p, curMonth: query.curMonth, active: query.active,
    }, { convert: true });
    if (result.error) {
        console.error(result.error);
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isInfoCorrect(info: string) {
    const infos = ['manager', 'password', 'project', 'role'];
    if (infos.indexOf(info) === -1) {
        throw new NotFoundError();
    }
    return info;
}

export function isNewPasswordCorrect(body: any): string {
    const newPassword = Joi.string().min(3).max(40).required()
        .messages({
            'string.max': 'Le mot de passe est trop long, max: 40',
            'string.min': 'Le mot de passe est trop court, min: 3',
        });
    const result = newPassword.validate(body.newPassword);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isUUID(id: string): string {
    const object = Joi.string().uuid().required().messages({
        'string.guid': 'L\'ID fournit n\'est pas correct',
        'any.required': 'Un champ obligatoire est manquant',
    });
    const result = object.validate(id);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isRoleCorrect(role : string) : string {
    const object = Joi.string().valid(cfg.roles.DEV, cfg.roles.ADMIN, cfg.roles.MANAGER).required().messages({
        'any.required': 'Un rôle est nécessaire',
        'any.valid': 'Le rôle est invalide',
    });
    const result = object.validate(role);
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}

export function isDateCorrect(date: string): Date {
    const object = Joi.date().less('now').required().messages({
        'any.required': 'Un rôle est nécessaire',
        'date.less': 'La date ne peut être dans le futur',
    });
    const result = object.validate(date, { convert: true });
    if (result.error) {
        throw new UnprocessableEntityError(result.error.message);
    }
    return result.value;
}
