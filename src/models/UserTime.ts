import {
    Entity, Column, BaseEntity, PrimaryColumn, JoinColumn, ManyToOne,
} from 'typeorm';
import { Project } from './Project';
import { User } from './User';

@Entity()
export class UserTime extends BaseEntity {
    // field
    @PrimaryColumn('varchar')
    id: string;
    @Column('int')
    time: number;
    @Column('date')
    date: Date;
    @Column('boolean')
    isLocked: boolean;
    @Column('varchar')
    projectId: string;
    @Column('varchar')
    userId: string;

    @ManyToOne(() => User, (entity) => entity.id, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'userId' })
    user!: User;
    @ManyToOne(() => Project, (entity) => entity.id, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'projectId' })
    project!: Project;

    // constructor
    constructor(id:string, time: number, date: Date, isLocked: boolean, userId: string, projectId: string) {
        super();
        this.id = id;
        this.time = time;
        this.date = date;
        this.isLocked = isLocked;
        this.userId = userId;
        this.projectId = projectId;
    }
}
