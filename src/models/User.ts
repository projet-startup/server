import {
    Entity, Column, BaseEntity, PrimaryColumn, Unique, OneToOne, JoinTable, ManyToMany,
} from 'typeorm';
// eslint-disable-next-line import/no-cycle
import { Project } from './Project';

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
    // field
    @PrimaryColumn('varchar')
    id: string;
    @Column('varchar')
    firstname: string;
    @Column('varchar')
    lastname: string;
    @Column('varchar')
    email: string;
    @Column('varchar')
    password: string;
    @Column('varchar')
    role: string;
    @Column('varchar', { nullable: true })
    managerId: string | null = null;

    @OneToOne(() => User, (entity) => entity.id, { onDelete: 'CASCADE' })
    @JoinTable({ name: 'managerId' })
    manager!: User;

    @ManyToMany(() => Project, (entity) => entity.developers, { onDelete: 'CASCADE' })
    @JoinTable()
    projects!: Project[];

    // constructor
    constructor(id: string, firstname: string, lastname: string, email: string, password: string, role: string, managerId?: string) {
        super();
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.role = role;
        if (managerId) this.managerId = managerId;
    }
}
