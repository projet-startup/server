import { TokenUser } from '../middleware/auth-middleware';

declare module 'express-serve-static-core' {
    interface Request {
        user?: TokenUser
    }
}
