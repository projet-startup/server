import {
    Entity, Column, BaseEntity, PrimaryColumn, ManyToMany,
} from 'typeorm';
// eslint-disable-next-line import/no-cycle
import { User } from './User';

@Entity()
export class Project extends BaseEntity {
    // field
    @PrimaryColumn('varchar')
    id: string;
    @Column('varchar')
    name: string;
    @Column('varchar')
    description: string;
    @Column('boolean')
    isOpen: boolean;

    @ManyToMany((type) => User, (entity) => entity.projects, { onDelete: 'CASCADE' })
    developers!: User[];

    // constructor
    constructor(id: string, name: string, description: string, isOpen: boolean) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.isOpen = isOpen;
    }
}
