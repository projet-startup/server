import { JsonWebTokenError, verify } from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import { readFileSync } from 'fs-extra';
import { UnauthorizedError, ForbiddenError } from '../core/error/http-error';
import * as cfg from '../config';
import userService from '../services/user-service';

export interface DecodedJwt {
    user: TokenUser,
    iat: number,
    exp: number
}

export interface TokenUser {
    role: string,
    firstname: string,
    lastname: string,
    email: string,
    id: string
}

export async function isAuthorized(req: Request, res: Response, next: NextFunction,
    roles: string[] = [cfg.roles.ADMIN, cfg.roles.DEV, cfg.roles.MANAGER]) {
    const token = req.headers.authorization?.split('Bearer ');
    if (!token || token.length < 2) {
        throw new UnauthorizedError('Veuillez vous connecter pour accéder à cette fonctionnalité');
    }
    let decoded: DecodedJwt;
    const cert = cfg.getjwtPassword() || readFileSync('private.key');
    try {
        decoded = verify(token[1], cert) as DecodedJwt;
        if (!roles.includes(decoded.user.role)) {
            throw new ForbiddenError('Accès non autorisé');
        }
        req.user = decoded.user;
        next();
    } catch (err) {
        throwError(err);
    }
}

export async function isManagerOrAdmin(req: Request, res: Response, next: NextFunction, id: string) {
    if (req.user?.role === cfg.roles.ADMIN) {
        next();
    }
    if (req.user?.role === cfg.roles.MANAGER) {
        const user = await userService.findById(id);
        if (user.managerId !== req.user.id) {
            throw new ForbiddenError('Vous ne pouvez pas modifier cet utilisateur');
        }
    }
}

function throwError(err: any) {
    if (err instanceof JsonWebTokenError) {
        throw new UnauthorizedError('Le token est invalide, essayer de vous reconnecter');
    }
    throw new ForbiddenError('Accès non autorisé');
}
