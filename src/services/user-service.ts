import { v4 as uuid } from 'uuid';
import { Secret, sign, SignOptions } from 'jsonwebtoken';
import { User } from '../models/User';
import * as cfg from '../config';
import { getConnection } from '../core/db-connections';
import {
    BadRequestError, ConflictError, ForbiddenError, NotFoundError,
} from '../core/error/http-error';
import crypto from 'crypto';

export class UserService {
    async create(user: User): Promise<string> {
        const copyUser = { ...user };
        const userRepository = (await getConnection()).getRepository(User);
        copyUser.id = uuid();
        if (copyUser.managerId) {
            await this.setManager(copyUser.id, copyUser.managerId);
        }
        copyUser.password = await this.hash(copyUser.password);

        try {
            await userRepository.save(copyUser);
        } catch (err) {
            if (err.errno === 1062) {
                throw new ConflictError('Email déjà utilisé');
            }
            throw err;
        }
        return copyUser.id;
    }

    async login(email: string, password: string) {
        const userRepository = (await getConnection()).getRepository(User);
        const user = await userRepository.createQueryBuilder('us')
            .leftJoinAndSelect('us.projects', 'pr')
            .where('us.email = :email', { email })
            .select(['us.id', 'us.firstname', 'us.lastname', 'us.email', 'us.password', 'us.managerId', 'us.role', 'pr.name', 'pr.id'])
            .getOne();
        if (!user) { throw new ForbiddenError('Utilisateur ou mot de passe incorrect'); }
        if (!(await this.verify(password, user.password))) throw new ForbiddenError('Utilisateur ou mot de passe incorrect');
        let token = '';
        if (user.role === cfg.roles.MANAGER) {
            token = await this.createJwt(user);
        } else {
            token = await this.createJwt(user);
        }
        const returnedUser: any = { ...user };
        delete returnedUser.password;
        return { user: { ...returnedUser }, token };
    }

    async setManager(id: string, managerId: string | null) {
        const userRepository = (await getConnection()).getRepository(User);
        const user = await userRepository.findOne(id);
        let manager = null;
        if (managerId) {
            manager = await userRepository.findOne(managerId);
        }
        if (!user || (!manager && managerId)) {
            throw new NotFoundError('Aucun utilisateur avec l\'ID fournit');
        }
        if (user.role !== cfg.roles.DEV || (manager && manager.role !== cfg.roles.MANAGER)) {
            throw new BadRequestError('Les rôles des utilisateurs ne sont pas compatibles');
        }
        user.managerId = managerId;
        await user.save();
    }

    async setPassword(id: string, newPassword: string) {
        const userRepository = (await getConnection()).getRepository(User);
        const user = await userRepository.findOne(id);
        if (!user) {
            throw new NotFoundError('Aucun utilisateur avec l\'ID fournit');
        }
        const hash = await this.hash(newPassword);
        await userRepository.update({ id }, { password: hash });
    }

    async setRole(id: string, newRole: string) {
        const userRepository = (await getConnection()).getRepository(User);
        const user = await userRepository.findOne(id);
        if (!user) {
            throw new NotFoundError('Aucun utilisateur avec l\'ID fournit');
        }
        if (user.role === newRole) {
            return;
        }
        if (newRole !== cfg.roles.DEV) {
            await userRepository.update({ id }, { managerId: null });
        } else {
            await userRepository.update({ managerId: id }, { managerId: null });
        }
        await userRepository.update({ id }, { role: newRole });
    }

    async findById(id: string): Promise<User> {
        const userRepository = (await getConnection()).getRepository(User);
        const user = await userRepository.createQueryBuilder('us')
            .leftJoinAndSelect('us.projects', 'pr')
            .select(['us.firstname', 'us.lastname', 'us.role', 'us.email', 'us.managerId', 'pr.name', 'pr.id'])
            .where({ id })
            .getOne();
        if (!user) { throw new NotFoundError('Aucun utilisateur avec l\'ID fournit'); }
        return user;
    }

    async findAll(): Promise<User[]> {
        const userRepository = (await getConnection()).getRepository(User);
        const users = await userRepository.createQueryBuilder('us')
            .leftJoinAndSelect('us.projects', 'pr')
            .select(['us.id', 'us.firstname', 'us.lastname', 'us.email', 'us.role', 'us.managerId', 'pr.name', 'pr.id'])
            .getMany();
        return users;
    }

    async update(user: User) {
        const userRepository = (await getConnection()).getRepository(User);
        try {
            const result = await userRepository.update({ id: user.id }, { firstname: user.firstname, lastname: user.lastname, email: user.email });
            if (result.affected === 0) {
                throw new NotFoundError();
            }
        } catch (err) {
            if (err.errno === 1062) {
                throw new ConflictError('Email déjà utilisé');
            }
            throw err;
        }
    }

    async delete(id: string) {
        const userRepository = (await getConnection()).getRepository(User);
        await userRepository.delete({ id });
    }

    async addToProject(id: string, projectId: string) {
        try {
            await (await getConnection()).createQueryBuilder().relation(User, 'projects').of(id).add(projectId);
        } catch (err) {
            if (err.errno === 1062) {
                throw new ConflictError('L\'utilisateur fait déjà partie du projet');
            }
            throw err;
        }
    }

    async removeFromProject(id: string, projectId: string) {
        try {
            await (await getConnection()).createQueryBuilder().relation(User, 'projects').of(id).remove(projectId);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    private createJwt(user: User): Promise<string> {
        return this.signJwt(
            {
                user: {
                    role: user.role,
                    email: user.email,
                    id: user.id,
                },
                date: Date.now(),
            },
            cfg.getjwtPassword(),
            {
                expiresIn: '2h',
            },
        );
    }

    private signJwt(payload: object, secret: Secret, options: SignOptions): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            sign(payload, secret, options, (err: Error | null, encoded: string | undefined) => {
                if (err || !encoded) {
                    reject(err);
                } else {
                    resolve(encoded);
                }
            });
        });
    }

    private async hash(password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            // generate random 64 bytes long salt
            const salt = crypto.randomBytes(32).toString('hex');

            crypto.scrypt(password, salt, 32, (err, derivedKey) => {
                if (err) reject(err);
                resolve(`${salt}:${derivedKey.toString('hex')}`);
            });
        });
    }

    private async verify(password: string, hash: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const [salt, key] = hash.split(':');
            crypto.scrypt(password, salt, 32, (err, derivedKey) => {
                if (err) reject(err);
                resolve(key === derivedKey.toString('hex'));
            });
        });
    }
}

export default new UserService();
