import { v4 as uuid } from 'uuid';
import { Project } from '../models/Project';
import { getConnection } from '../core/db-connections';
import { ConflictError, NotFoundError } from '../core/error/http-error';
import { User } from '../models/User';

export class ProjectService {
    async create(project: Project) {
        project.id = uuid();
        project.isOpen = true;
        const projectRepository = (await getConnection()).getRepository(Project);
        await projectRepository.save(project);
        return project.id;
    }

    async findById(id: string): Promise<Project> {
        const projectRepository = (await getConnection()).getRepository(Project);
        const project = await projectRepository.findOne(id);
        if (!project) { throw new NotFoundError("Aucun projet ne correspond à l'ID fournit"); }
        return project;
    }

    async findAll(): Promise<Project[]> {
        const projectRepository = (await getConnection()).getRepository(Project);
        const projects = await projectRepository.find();
        return projects;
    }

    async update(project: Project) {
        const projectRepository = (await getConnection()).getRepository(Project);
        try {
            const result = await projectRepository.update({ id: project.id, isOpen: true },
                { name: project.name, description: project.description });
            if (result.affected === 0) {
                throw new NotFoundError("Aucun projet ouvert ne correspond à l'ID fournit");
            }
        } catch (err) {
            if (err.errno === 1062) {
                throw new ConflictError('Un projet existe déjà avec ce nom');
            }
            throw err;
        }
    }

    async delete(id: string) {
        const projectRepository = (await getConnection()).getRepository(Project);
        await projectRepository.delete({ id });
    }

    async changeOpen(id: string, isOpen: boolean) {
        const projectRepository = (await getConnection()).getRepository(Project);
        await projectRepository.update({ id }, { isOpen });
        if (!isOpen) {
            await (await getConnection()).createQueryBuilder().relation(User, 'projects').remove(id);
        }
    }

    async findDevelopers(id: string): Promise<User[]> {
        const projectRepository = (await getConnection()).getRepository(Project);
        const query = projectRepository.createQueryBuilder('pr')
            .leftJoinAndSelect('pr.developers', 'us')
            .where('pr.id = :id', { id })
            .select(['pr.id', 'us.id', 'us.firstname', 'us.lastname', 'us.email']);
        try {
            const project = await query.getOne();
            return project?.developers || [];
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
}

export default new ProjectService();
