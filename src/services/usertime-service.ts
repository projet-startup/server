import { v4 as uuid } from 'uuid';
import { UserTime } from '../models/UserTime';
import { getConnection } from '../core/db-connections';
import {
    ConflictError, ForbiddenError, NotFoundError, UnprocessableEntityError,
} from '../core/error/http-error';
import fs from 'fs-extra';
import Path from 'path';
import Puppeteer from 'puppeteer';
import Handlebars from 'handlebars';
import userService from './user-service';
import { User } from '../models/User';
import { Project } from '../models/Project';

export class UserTimeService {
    private pageLength = 20;

    async create(usertime: UserTime) {
        usertime.id = uuid();
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        if (await this.isMonthLocked(usertime.userId, usertime.date)) {
            throw new ForbiddenError('Vous ne pouvez pas créer un pointage sur ce mois');
        }
        try {
            await userTimeRepository.save(usertime);
        } catch (err) {
            if (err.errno === 1452) {
                throw new NotFoundError('Aucun projet ne correspojnd a l\'ID fournit');
            }
            throw err;
        }
        return usertime.id;
    }

    async isMonthLocked(id: string, date: Date) {
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        const startMonth = new Date(date.getFullYear(), date.getMonth(), 1);
        const end = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        const query = await userTimeRepository.createQueryBuilder('ut')
            .where('ut.userId = :id', { id })
            .andWhere('ut.date >= :startMonth', { startMonth })
            .andWhere('ut.date < :end', { end })
            .andWhere('ut.isLocked = :locked', { locked: true })
            .getMany();
        if (query.length > 0) {
            return true;
        }
        return false;
    }

    async findById(id: string): Promise<UserTime> {
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        const userTime = await userTimeRepository.findOne(id);
        if (!userTime) { throw new NotFoundError('Aucun pointage ne correspojnd a l\'ID fournit'); }
        return userTime;
    }

    async findAll(): Promise<UserTime[]> {
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        const userTimes = await userTimeRepository.find();
        return userTimes;
    }

    async findByUser(id: string, options: { page: number, active: boolean, curMonth: boolean } =
    { page: 1, active: true, curMonth: false }): Promise<Project[]> {
        const projectRepository = (await getConnection()).getRepository(Project);
        let query = projectRepository.createQueryBuilder('pr')
            .leftJoinAndMapMany('pr.usertimes', UserTime, 'ut', 'ut.projectId = pr.id')
            .select(['ut.id', 'ut.date', 'ut.time', 'ut.isLocked', 'pr.id', 'pr.name'])
            .where('ut.userId = :id', { id });
        if (options.curMonth) {
            const date = new Date();
            const start = new Date(date.getFullYear(), date.getMonth(), 1);
            query = query
                .andWhere('ut.date >= :start', { start });
        }
        query = query.skip((options.page - 1) * this.pageLength)
            .take(this.pageLength)
            .orderBy('ut.date', 'DESC');
        const userTimes = await query.getMany();
        return userTimes;
    }

    async findActiveByUser(id: string, options: { page: number, curMonth: boolean } =
    { page: 1, curMonth: false }): Promise<Project[]> {
        const userRepository = (await getConnection()).getRepository(User);
        let query = userRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.projects', 'pr')
            .leftJoinAndMapMany('pr.usertimes', UserTime, 'ut', 'ut.projectId = pr.id')
            .where('user.id = :id', { id })
            .andWhere('ut.userId = user.id')
            .select(['ut.id', 'user.id', 'user.managerId', 'user.firstname', 'user.lastname',
                'ut.id', 'ut.date', 'ut.time', 'ut.isLocked', 'pr.id', 'pr.name']);

        if (options.curMonth) {
            const date = new Date();
            const start = new Date(date.getFullYear(), date.getMonth(), 1);
            query = query.where('ut.date >= :start', { start });
        }
        const userTimes = await query.getOne();
        return userTimes?.projects || [];
    }

    async findByUserChrono(id: string, options: { page: number, active: boolean, curMonth: boolean } =
    { page: 1, active: true, curMonth: false }): Promise<UserTime[]> {
        const projectRepository = (await getConnection()).getRepository(UserTime);
        let query = projectRepository.createQueryBuilder('ut')
            .leftJoinAndSelect('ut.project', 'pr')
            .select(['ut.id', 'ut.date', 'ut.time', 'ut.isLocked', 'pr.id', 'pr.name'])
            .where('ut.userId = :id', { id });
        if (options.curMonth) {
            const date = new Date();
            const start = new Date(date.getFullYear(), date.getMonth(), 1);
            query = query
                .andWhere('ut.date >= :start', { start });
        }
        query = query.skip((options.page - 1) * this.pageLength)
            .take(this.pageLength)
            .orderBy('ut.date', 'DESC');
        const userTimes = await query.getMany();
        return userTimes;
    }

    async findByManager(id: string, options: { page: number, curMonth: boolean }
    = { page: 1, curMonth: false }): Promise<User[]> {
        const userRepository = (await getConnection()).getRepository(User);
        let query = userRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.projects', 'pr')
            .leftJoinAndMapMany('pr.usertimes', UserTime, 'ut', 'ut.projectId = pr.id')
            .where('user.managerId = :id', { id })
            .andWhere('ut.userId = user.id')
            .select(['ut.id', 'user.id', 'user.managerId', 'user.firstname', 'user.lastname',
                'ut.id', 'ut.date', 'ut.time', 'ut.isLocked', 'pr.id', 'pr.name']);

        if (options.curMonth) {
            const date = new Date();
            const start = new Date(date.getFullYear(), date.getMonth(), 1);
            query = query.where('ut.date >= :start', { start });
        }
        const userTimes = query
            .skip((options.page - 1) * this.pageLength)
            .take(this.pageLength)
            .orderBy('user.id')
            .getMany();

        return userTimes;
    }

    async findByProject(id: string, options: { page: number, chrono: boolean, curMonth: boolean }
    = { page: 1, chrono: false, curMonth: false }): Promise<Project | undefined> {
        const projectRepository = (await getConnection()).getRepository(Project);
        const query = projectRepository.createQueryBuilder('pr')
            .leftJoinAndSelect('pr.developers', 'user')
            .leftJoinAndMapMany('user.userTime', UserTime, 'ut', 'ut.userId = user.id')
            .select(['ut.id', 'ut.date', 'ut.time', 'ut.isLocked', 'pr.id', 'pr.name', 'user.firstname', 'user.id', 'user.lastname'])
            .where('pr.id = :id', { id })
            .andWhere('ut.projectId = pr.id');
        let userTimes;
        if (options.curMonth) {
            const date = new Date();
            const start = new Date(date.getFullYear(), date.getMonth(), 1);
            userTimes = await query
                .where('ut.date >= :start', { start })
                .skip((options.page - 1) * this.pageLength)
                .take(this.pageLength)
                .orderBy('ut.date')
                .getOne();
        } else {
            userTimes = await query
                .skip((options.page - 1) * this.pageLength)
                .take(this.pageLength)
                .orderBy('ut.date')
                .getOne();
        }
        return userTimes;
    }

    async update(userTime: UserTime) {
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        try {
            const result = await userTimeRepository.update({ id: userTime.id, isLocked: false },
                { time: userTime.time, date: userTime.date });
            if (result.affected === 0) {
                throw new NotFoundError();
            }
        } catch (err) {
            if (err.errno === 1062) {
                throw new ConflictError('Le pointage n\'existe pas ou ne peut plus être modifié');
            }
            throw err;
        }
    }

    async delete(id: string) {
        const userTimeRepository = (await getConnection());
        const query = userTimeRepository.createQueryBuilder().delete().from(UserTime)
            .where('id = :id', { id })
            .andWhere('isLocked = :isLocked', { isLocked: false });
        const res = await query.execute();
        if (res.affected === 0) {
            throw new NotFoundError('Le pointage n\'existe pas ou a déjà été validé');
        }
    }

    async validate(uid: string, date: Date): Promise<number> {
        const start = new Date(date.getFullYear(), date.getMonth(), 1);
        const end = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        const userTimeRepository = await getConnection();
        const query = userTimeRepository.createQueryBuilder()
            .update(UserTime)
            .set({ isLocked: true })
            .where('userId = :uid', { uid })
            .andWhere('date >= :start', { start })
            .andWhere('date < :end', { end });
        const res = await query.execute();
        return res.affected || 0;
    }

    async generatePDF(uid: string, date: Date): Promise<any> {
        await (this.validate(uid, date));
        const start = new Date(date.getFullYear(), date.getMonth(), 1);
        const end = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        const userTimeRepository = (await getConnection()).getRepository(UserTime);
        const query = userTimeRepository.createQueryBuilder('ut').leftJoinAndSelect('ut.user', 'user').leftJoinAndSelect('ut.project', 'project')
            .select()
            .where('ut.userId = :uid', { uid })
            .andWhere('ut.date >= :start', { start })
            .andWhere('ut.date < :end', { end })
            .orderBy('ut.date');
        const res = await query.execute();
        if (res.length === 0) {
            throw new UnprocessableEntityError('Aucun pointage pour le mois sélectionné');
        }
        const manager = await userService.findById(res[0].user_managerId);
        const docName = 'test.pdf';
        const tableContent = [];
        let total = 0;
        for (let i = 0; i < res.length; i++) {
            const object = {
                project: res[i].project_name,
                date: new Date(res[i].ut_date).toLocaleString('default', {
                    weekday: 'long', day: 'numeric', month: 'long', year: 'numeric',
                }),
                time: res[i].ut_time,
            };
            total += res[i].ut_time;
            tableContent.push(object);
        }
        const data = {
            user_firstname: res[0].user_firstname,
            user_lastname: res[0].user_lastname,
            manager_firstame: manager.firstname,
            manager_lastame: manager.lastname,
            month: new Date(res[0].ut_date).toLocaleString('default', { month: 'long', year: 'numeric' }),
            table_content: tableContent,
            total,
        };

        const html = await this.loadTemplate({ data });

        const browser = await Puppeteer.launch();
        const page = await browser.newPage();
        await page.setContent(html);
        await page.pdf({ path: docName });
        return docName;
    }

    async loadTemplate(data : Object) : Promise<any> {
        try {
            const templateHtml = fs.readFileSync(Path.join(process.cwd(), 'template.html'), 'utf8');
            const template = Handlebars.compile(templateHtml);
            return template(data);
        } catch (error) {
            console.error(error);
            throw new Error('Cannot create invoice HTML template.');
        }
    }
}

export default new UserTimeService();
