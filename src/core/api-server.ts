import express, {
    Request, Response, NextFunction, Router,
} from 'express';
import http from 'http';
import helmet from 'helmet';
import cors from 'cors';
import { HttpError } from './error/http-error';
import * as cfg from '../config';
import { Controller } from './controller';
import userController from '../controllers/user-controller';
import authController from '../controllers/auth-controller';
import projectController from '../controllers/project-controller';
import usertimeController from '../controllers/usertime-controller';

export class ApiServer {
    private controllers: Controller[] = [userController, authController, projectController, usertimeController];
    public run() {
        const app = express();
        const server = http.createServer(app);

        app.use(helmet());
        app.use(cors());
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        this.registerControllers(app);
        app.get('/test', (req, res, next) => { res.send({ message: 'test' }); });
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            if (err instanceof HttpError && err.status !== 500) {
                res.status(err.status).send({ message: err.title, code: err.code }).end();
            } else {
                console.error(err);
                res.status(500).send({ message: 'Une erreur est survenue', code: 'INTERNAL_SERVER_ERROR' });
            }
        });

        server.listen(cfg.getServerPort(), () => {
            console.log(`App listening on port ${cfg.getServerPort()}`);
        });
    }

    private registerControllers(app: Router): void {
        const router = express.Router();
        this.controllers.forEach((controller) => {
            app.use('/api', router);
            controller.initRoutes(router);
        });
    }
}

export default new ApiServer();
