import { getDbUri } from '../config';
import 'reflect-metadata';
import { Connection, createConnection } from 'typeorm';
import { User } from '../models/User';
import { Project } from '../models/Project';
import { UserTime } from '../models/UserTime';

let connection : Connection;

export async function getConnection(): Promise<Connection> {
    if (!connection) {
        connection = await createConnection({
            type: 'mysql',
            url: getDbUri(),
            entities: [
                User, Project, UserTime,
            ],
        });
        await connection.synchronize();
    }
    return connection;
}

export async function close() {
    if (connection) {
        try {
            await connection.close();
        } catch (err) { console.error(err); }
    }
}
